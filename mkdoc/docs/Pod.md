

## Pod

Un pod es uno o mas contenedores que comparten namespaces entre si.

Los contenedores internos de un pod comparen NCT, IPC, UTS. es decir comparten ip, hostname, y procesos.

Para crear los contenedores del pod, primero crea un contenedor de configuración a este le extrae el id, y a partir de este id crea los contenedores necesarios, de esta forma pueden compartir la red, hostname y procesos entre ellos. para finalizar la creación del pod elimina el container de configuración.

**comparten**: IPC inter process comunication, Network, UTS  Unix timesharing system

**no comparten**: mount, PID, USER, Cgroup.

El no compartir el mount, permite decidir que montar y no montar en cada contenedor, y el no compartir Cgroup da control sobre la cpu que consume cada container.



## Gestionar pod manualmente

https://kubernetes.io/docs/reference/kubectl/conventions/

Al crear un pod hay que tener en cuenta la versión de la api que estamos utilizando y que la imagen desde la que creará el contenedor la cogerá de docker.

También hay que tener en cuenta, que crear un pod desde linea de comandos es una mala practica, lo recomendable es utilizar manifiestos o un objeto de mas alto nivel, ya que pod por si mismo no puede auto ejecutarse, por lo tanto si muere se tendrá que arrancar manualmente otra vez.

**Importante**: En el caso de que un container de un pod fallara, el pod se crea, pero no da datos de que a fallado, en ese caso se tendrá que inspeccionar el pod con `kubectl describe pod <nombre-pod> o kubectl logs <nombre-pod>`



```bash
➜  sudo kubectl api-versions # ver versiónnde kubectl 
➜  ~ sudo kubectl run --generator=run-pod/v1 podtest --image=nginx:alpine
pod/podtest created

➜  ~ sudo kubectl get pods                                          
NAME      READY   STATUS    RESTARTS   AGE
podtest   1/1     Running   0          12s
```

- con `get pods` se puede ver como a creado el pod y su estado es corriendo, en la columna `READY` se ve que esta corriendo un contenedor de uno, ya que en un pod puede haber diferentes contenedores.



### Comandos de gestion en pods

```bash
# crear pod
➜  sudo kubectl run --generator=run-pod/v1 podtest --image=nginx:alpine
➜  sudo kubectl delete pod podtest1	# eliminar pod

# informativos
➜  sudo kubectl describe  pod podtest # informacion del pod
➜  sudo kubectl get pod podtest -o yaml # extraer info de pot en yaml
➜  sudo kubectl api-resources # diferentes recursos de la api

# entrada
➜  sudo kubectl exec -it podtest -- sh # entrar pod con 1 contenedor
➜  sudo kubectl exec -it doscont -c contenedor2 -- sh # entrar contenedor2 del pod doscont

# logs
➜  sudo kubectl logs podtest    # ver logs
➜  sudo kubectl logs podtest -f # vre logs interactivos
➜  sudo kubectl logs doscont -c contenedor1 # ver logs del contendor1

# manifiestos
➜  sudo kubectl apply -f pod.yaml  # aplicar manifiesto
➜  sudo kubectl delete -f pod.yaml # eliminar manifiesto
```



### Manifiestos

Un manifiesto es un archivo en yaml , yml o json que define el recurso que queremos crear o actualizar en kubernetes, la ventaja que proporciona esta practica, es que, se puede desplegar uno o mas pods a la vez y estos teniendo uno o mas contenedores en su interior.

https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/

#### Simple

Ejemplo de manifiesto muy simple, con un pod podtest2 y un contenedor de una imagen nginx.

*pod.yml*

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: podtest2
spec:
  containers:
  - name: contenedor1
    image: nginx:alpine
```



Desplegar manifiesto.

```bash
➜  pods sudo kubectl apply -f pod.yaml 
pod/podtest2 created
➜  pods sudo kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
podtest2   1/1     Running   0          15s

➜  pods sudo kubectl delete -f pod.yaml
```



#### Dos pods

En este ejemplo de manifiesto se crean dos pods, para identificar donde acaba un pod y donde empieza el siguiente se define con `---` .

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: podtest2
spec:
  containers:
  - name: contenedor1
    image: nginx:alpine
---
apiVersion: v1
kind: Pod
metadata:
    name: podtest3
spec:
    containers:
    - name: contenedor1
      image: nginx:alpine
```



Se puede ver como al cargarlo se crean  dos pods a la vez.

```bash
➜  pods sudo kubectl apply -f pod.yaml
pod/podtest2 unchanged
pod/podtest3 created
➜  pods sudo kubectl get pods
NAME       READY   STATUS    RESTARTS   AGE
podtest2   1/1     Running   0          45s
podtest3   1/1     Running   0          19s

➜  pods sudo kubectl delete -f pod.yaml 
pod "podtest2" deleted
pod "podtest3" deleted
```



#### Dos contenedores

Al crear dos contenedores en el mismo pod se tiene que tener en cuenta, que estos dos contenedores están compartiendo ip,  pero nunca se puede compartir puerto.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: doscont
spec:
  containers:
  - name: contenedor1
    image: python:3.6-alpine
    command: ['sh', '-c', 'echo "contenedor1" > index.html && python -m http.server 8082']
  - name: contenedor2
    image: python:3.6-alpine
    command: ['sh', '-c', 'echo "contenedor2" > index.html && python -m http.server 8083']
```



Cargar manifiesto.

```bash
➜  pods sudo kubectl apply -f doscont.yaml 
pod/doscont created

➜  pods sudo kubectl get pods             
NAME      READY   STATUS    RESTARTS   AGE
doscont   2/2     Running   0          8s
```



n el caso que un contenedor fallara, se vería de esta manera, `READY 1/2` 

```bash
➜  pods sudo kubectl get pods                   
NAME      READY   STATUS             RESTARTS   AGE
doscont   1/2     CrashLoopBackOff   6          7m58s
```



#### Labels

Los labels juegan un papel importante en los pods, esto es por dos razones:

- Los objetos de mas alto nivel se pueden referir a los pods mediante labels, es decir puedo indicar aumenta las replicas de los backend, siendo backend un label.
- Se puede filtrar el output de pods mediante labels.



La sección de label se define dentro de metadata, y dentro de label, las variables que indiques son a tu gusto, aun que una buena practica es definir app como mínimo.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: podtest2
  labels:
    app: front
    env: dev
spec:
  containers:
  - name: contenedor1
    image: nginx:alpine
```



Ejemplo de filtro.

```bash
➜  sudo kubectl get pods -l app=backend
NAME       READY   STATUS    RESTARTS   AGE
podtest3   1/1     Running   0          46s
➜  sudo kubectl get pods -l env=dev    
NAME       READY   STATUS    RESTARTS   AGE
podtest2   1/1     Running   0          5m54s
podtest3   1/1     Running   0          5m54s
```


