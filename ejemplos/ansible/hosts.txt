[cluster]
192.168.122.200
192.168.122.201

[master]
192.168.122.200

[workers]
192.168.122.201


[cluster:vars]
ansible_python_interpreter=/usr/bin/python3

[master:vars]
ansible_python_interpreter=/usr/bin/python3

[workers:vars]
ansible_python_interpreter=/usr/bin/python3
