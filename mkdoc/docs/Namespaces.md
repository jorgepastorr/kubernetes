
## Namespaces

Namespaces son espacios de trabajo dentro de un cluster, donde cada namespace es independiente del otro, es decir los services, deployments, replicasets y pods no se ven entre cada espacio de trabajo. esto es especialmente bueno para trabajar con diferentes departamentos de trabajo, como desarrolladores, sysadmin, ... Otra ventaja es que se puede limitar los espacios de trabajo en los recursos que podrán consumir cada uno de ellos y también en acceso.

Por defecto kubernetes tiene los espacios de trabajo que no debemos tocar `kube-public, kube-system y kube-node-lease` estos espacios de trabajo los utiliza kubernetes y no es buena practica trastear en ellos. El usuario o administrador trabaja  en el namespace default o uno creado para un grupo especifico.

```bash
sudo kubectl get namespaces              
NAME              STATUS   AGE
default           Active   15d
kube-node-lease   Active   15d
kube-public       Active   15d
kube-system       Active   15d
```



Comandos 

```bash
sudo kubectl get namespaces    # visualizar
sudo kubectl create namespace test-ns # crear
sudo kubectl delete namespace test-ns # eliminar
sudo kubectl describe namespaces test-ns # inspeccionar
# ver objeto de namespace -n abreviacion
sudo kubectl get pods|replicasets|deployments|services --namespace test-ns
sudo kubectl get pods|replicasets|deployments|services -n test-ns
# ver de todos los namespaces -A abreviacion de all-namespaces
sudo kubectl get pods|replicasets|deployments|services -A
```



### Gestión manual

```bash
# Crear namespace simple
sudo kubectl create namespace test-ns
namespace/test-ns created

sudo kubectl get namespaces              
NAME              STATUS   AGE
default           Active   15d
kube-node-lease   Active   15d
kube-public       Active   15d
kube-system       Active   15d
test-ns           Active   5s

# ver contenido namspace
sudo kubectl describe namespaces test-ns
Name:         test-ns
Labels:       <none>
Annotations:  <none>
Status:       Active

No resource quota.
No LimitRange resource.
```

**resource quota**: Es la limitación de cierta cantidad de recursos, que el namespace no va a ser sobrepasar.

**limitRange resource**: Es una manera de controlar recursos a nivel de objetos individuales. Es decir, pods, deployments, replicasets, ...



#### Ejemplo espacios de trabajo

```bash
# creo un pod en el namespace test-ns
sudo kubectl run   --generator=run-pod/v1 podtest --image=nginx:alpine --namespace test-ns 
pod/podtest created

# desde el namespace default no hay nada
sudo kubectl get pods
No resources found in default namespace.

# pero en el namespace test-ns se a creado
sudo kubectl get pods -n test-ns
NAME      READY   STATUS    RESTARTS   AGE
podtest   1/1     Running   0          26s

sudo kubectl delete pod podtest -n test-ns
pod "podtest" deleted

sudo kubectl delete namespaces test-ns    
namespace "test-ns" deleted
```



#### Template

En el siguiente templte se crean dos namespaces, con un deployment en cada uno. Para indicar que un deployment pertenece a un namespace, se le añade en el deployment la metadata `namespace: nombre-del-namespace` .

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: dev
  labels:
    name: dev
---
apiVersion: v1
kind: Namespace
metadata:
  name: prod
  labels:
    name: prod
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kubernetes.io/change-cause: "version inicial"
  name: deployment-test
  namespace: dev
  labels:
    app: front-nginx
spec:
  replicas: 2
  selector:
    matchLabels:
      app: front-nginx
  template:
    metadata:
      labels:
        app: front-nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
---
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    kubernetes.io/change-cause: "version inicial"
  name: deployment-prod
  namespace: prod
  labels:
    app: front-nginx
spec:
  replicas: 4
  selector:
    matchLabels:
      app: front-nginx
  template:
    metadata:
      labels:
        app: front-nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
```



Se pude ver como se crea en cada namespace su deployment con sus pods independientes del cada espacio de trabajo.

```bash
sudo kubectl apply -f  namespaces/envs-ns.yml
namespace/dev unchanged
namespace/prod unchanged
deployment.apps/deployment-test created
deployment.apps/deployment-prod created

# en el namespace default no hay nada
sudo kubectl get deploy     
No resources found in default namespace.

# el namespace dev esta corriendo el deployment con sus pods
sudo kubectl get deploy -n dev
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
deployment-test   2/2     2            2           81s

sudo kubectl get pods -n dev 
NAME                               READY   STATUS    RESTARTS   AGE
deployment-test-694b78cb4c-2lfp9   1/1     Running   0          2m7s
deployment-test-694b78cb4c-ddcv4   1/1     Running   0          2m7s

# en el namespace prod corre su deploy y sus pods
sudo kubectl get deploy -n prod
NAME              READY   UP-TO-DATE   AVAILABLE   AGE
deployment-prod   4/4     4            4           99s

sudo kubectl get pods -n prod 
NAME                               READY   STATUS    RESTARTS   AGE
deployment-prod-694b78cb4c-c8w4f   1/1     Running   0          2m
deployment-prod-694b78cb4c-f5bx6   1/1     Running   0          2m
deployment-prod-694b78cb4c-w57ch   1/1     Running   0          2m
deployment-prod-694b78cb4c-z66ln   1/1     Running   0          2m

# la opcion all-namespaces muestra todo
sudo kubectl get deploy -A
NAMESPACE     NAME              READY   UP-TO-DATE   AVAILABLE   AGE
dev           deployment-test   2/2     2            2           6m59s
kube-system   coredns           2/2     2            2           15d
prod          deployment-prod   4/4     4            4           6m59s
```



#### Dns entre namespaces

El dns en un namespace se crea automáticamente y los pods de un mismo espacio de trabajo se ven entre ellos, pero entre espacios de trabajo solo se podran ver a nivel de servicios y especificando el FQDN de la siguiente manera.

`service-name + namespace-name + svc.cluster.local`



Ejemplo de dns entre  namespaces.

```bash
sudo kubectl get services -n dev
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
my-service   ClusterIP   10.98.97.69   <none>        8080/TCP   15m

sudo kubectl run -ti --generator=run-pod/v1 podtest --image=nginx:alpine --namespace default -- sh 
# no esta en el mismo namespace y no lo ve
/ # curl my-service:8080
curl: (6) Could not resolve host: my-service

# pero con el FQDN llegan a verse.
/ # curl my-service.dev.svc.cluster.local:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
```



### Contexto

El contexto en kubernetes es el espacio de trabajo en el que trabaja un usuario. 

Un usuario por defecto trabaja en el namespace default y si es un desarrollador y tiene su namespace desarrollo, tiene que estar indicando siempre `-n desarrollo `  o  ` --namespace desarrollo` y esto es engorroso.

Para esto mismo existen los contextos, para asignarte un namespace por defecto en un usuario.

```bash
# contexto actual
sudo kubectl config current-context
minikube

# añadier nuevo contexto context-dev con namespace por defecto dev
sudo kubectl config set-context context-dev --namespace=dev \
  --cluster=minikube \
  --user=minikube
 
# usar contexto
sudo kubectl config use-context context-dev
Switched to context "context-dev".

# verificar
sudo kubectl config current-context        
context-dev

# ahora por defecto muestra los objetos del namespace dev
sudo kubectl get svc 
NAME         TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
my-service   ClusterIP   10.98.97.69   <none>        8080/TCP   35m
```

