
## Servicios & endpoints

El **objeto services** es el encargado de balancear la carga entre los diferentes pods, esto lo hace a trabes de los labels para identificar a que pods de estar observando, no importa si esos pods están en un replicaset u otro, solo mira labels de pods y hay que tener en cuenta eso.

El balanceo de carga sirve (en el caso de una web ) para aumentar las peticiones que puede llegar a recibir al mismo tiempo, ya que se distribuirán entre los múltiples pods en vez de uno solo. es decir un cliente hace la petición a una ip y el objeto services se encarga a redirigir esa petición al pod indicado.

El **endpoint** de un servicio es el encargado de guardar la lista de ip's de los pods a consultar, en el caso de que un pod muera y se arranque otro, borrara la ip del pod muerto y añadirá la del pod nuevo.



Un servicio siempre esta asociado a una serie de pods, por lo tanto creo un deployment con pods nginx y lo asocio a el servicio, en el que expongo el puerto 80 de los pods al 8080 de la ip virtual del servicio.

Ejemplo de servicio:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-test
  labels:
    app: front-nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: front-nginx
  template:
    metadata:
      labels:
        app: front-nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
---
apiVersion: v1
kind: Service
metadata:
  name: my-service
  labels:
    app: front-nginx
spec:
  type: ClusterIP
  selector:
    app: front-nginx
  ports:
    - protocol: TCP
      port: 8080
      targetPort: 80
```



Desplegar servicio.

```bash
sudo kubectl apply -f service/service.yaml  
deployment.apps/deployment-test created
service/my-service created

sudo kubectl get svc -l app=front-nginx
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
my-service   ClusterIP   10.99.212.24   <none>        8080/TCP   93s
```



Descripción del servicio, donde se ve que a creado su endpoint, con las ips de los pods que balanceará la carga.

```bash
# descripción del servicio creado
sudo kubectl describe service my-service
Name:              my-service
Namespace:         default
Labels:            app=front-nginx
Annotations:       kubectl.kubernetes.io/last-applied-configuration:
                     {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{},"labels":{"app":"front-nginx"},"name":"my-service","namespace":"default"}...
Selector:          app=front-nginx
Type:              ClusterIP
IP:                10.99.212.24
Port:              <unset>  8080/TCP
TargetPort:        80/TCP
Endpoints:         172.17.0.4:80,172.17.0.5:80,172.17.0.6:80
Session Affinity:  None
Events:            <none>

# crea automaticamente su endpoint
sudo kubectl get endpoints
NAME         ENDPOINTS                                   AGE
kubernetes   192.168.88.2:8443                           9d
my-service   172.17.0.4:80,172.17.0.5:80,172.17.0.6:80   7m31s

# pods con ip asociadas al deployment
sudo kubectl get pods -o wide
NAME                              READY   STATUS    RESTARTS   AGE     IP           NODE   NOMINATED NODE   READINESS GATES
deployment-test-5699757d6-77ps4   1/1     Running   0          8m18s   172.17.0.5   pc02   <none>           <none>
deployment-test-5699757d6-kk96q   1/1     Running   0          8m18s   172.17.0.6   pc02   <none>           <none>
deployment-test-5699757d6-xb2rc   1/1     Running   0          8m18s   172.17.0.4   pc02   <none>           <none>
```



### Tipos de servicios

**clusterip** es la opción por defecto y consiste en una ip virtual que se le asigna al servicio, esta ip es interna, es decir desde mi ip local no podre ver el servicio, solo podre ver mi servicio indicando localmente la ip virtual que se le a añadido a dicho cluster.



**nodeport** es una capa superior a clusterip, y proporciona una redirección a la ip local del host ( a localhost no, solo la local ). por defecto asigna un puerto aleatorio entre el rango 30000-32767 , a no ser que se le indique manualmente en que puerto salir, eso si, entre el rango permitido de 30000-32767.

En el siguiente ejemplo se ve como a redireccionado automáticamente al puerto  local `30425` .

```bash
sudo kubectl get svc                      
NAME               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes         ClusterIP   10.96.0.1      <none>        443/TCP          9d
my-service         ClusterIP   10.99.212.24   <none>        8080/TCP         57m
my-service-nginx   NodePort    10.99.15.134   <none>        8080:30425/TCP   20m
```

Ejemplo de nodeport con redirección manual de puerto local

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service-nginx
  labels:
    app: backend-nginx
spec:
  type: NodePort
  selector:
    app: backend-nginx
  ports:
    - protocol: TCP
      nodePort: 30001
      port: 8080
      targetPort: 80
```



**LoadBalancer** Ee un tipo de balanceador de carga desarrollado para el cloud, este servicio estaría funcionando en la nube y balancea la carga a diferentes nodos, donde cada nodo se crea automaticamente su NodePort y ClusterIp
