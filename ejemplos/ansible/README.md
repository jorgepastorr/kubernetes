# Ansible & kubernetes

En este directorio se encuentran archivos para desplegar un cluster Kubernetes en dos máquinas virtuales, donde el master tiene la ip `192.168.122.200` y el nodo `192.168.122.201` se puede ver reflejado en el archivo `hosts.txt`.

Estas máquinas virtuales previamente se les a asignado un ip fija, instalado docker y asignado un hostname, además se tiene acceso mediante ssh con el certificado público ( que no pida contraseña ) desde el host que se ejecutara ansible

En este cluster el master también hace de worker, por lo tanto tendremos dos nodos ejecutando aplicaciones.

Modo de ejecución:

```bash
ansible-playbook setup-cluster.yml -i hosts.txt -u root
```
